const admin = require('firebase-admin');
const express = require('express');
const { User, Event } = require('../database/index');

module.exports = () => {
    
    const router = express.Router();

    router.get('/', (req, res) => {
        const _emailId = req.useremail;
        User.findOne({
            where: { email: _emailId },
            include: [{
                as: 'events',
                model: Event,
                attributes: ['id', 'name'],
                through: {
                    attributes: [],
                }
            }]
        }).then((user) => {
            if(user) {
                res.status(200).json(user.toJSON());
            } else {
                res.sendStatus(404);
            }
        })
    })

    router.put('/', (req, res) => {
        const _emailId = req.useremail;
        User.findByPk(_emailId).then(async (data) => {
            if(data) {
                data.update(req.body);
                res.sendStatus(200);
            } else {
                User.create({
                    email: _emailId,
                    ...req.body
                }).then((user) => {
                    res.sendStatus(201);
                });
            }
        }).catch((err) => {
            console.error(err);
        })
    });

    return router;

}