const express = require('express');
const { User, Event } = require('../database/index');


module.exports = {
    unauthorized: () => {
    
        const router = express.Router();
    
        router.get('/', (req, res) => {
            Event.findAll().then((events) => {
                res.status(200).json({
                    events
                })
            })
        });

        return router;
    
    },
    
    authorized: () => {
    
        const router = express.Router();
    
        router.post('/:id/register', (req, res) => {
            const eventId = req.params.id;
            const _emailId = req.useremail;
            Event.findByPk(eventId)
                .then((e) => {
                    User.findOne({
                        where: {email: _emailId}
                    }).then((u) => {
                        u.addEvents(e);
                        res.sendStatus(200);
                    })
                })
        })
    
        return router;
    }
}