const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet')

const admin = require('firebase-admin');
require('dotenv').config()
require('./database');

const userRoutes = require('./api/user.api');
const eventRoutes = require('./api/event.api');

const { fetchEmailFromToken } = require('./middleware');

const app = express();

app.use(cors());
app.use(helmet());
app.use(bodyParser.json());

admin.initializeApp({
    credential: admin.credential.cert(require('./psg-kriya-2021-firebase-key.json'))
});

app.use('/event', eventRoutes.unauthorized());

app.use(fetchEmailFromToken);

app.use('/event', eventRoutes.authorized());
app.use('/user', userRoutes());

const port = process.env.PORT || 3003
const server = app.listen(port, () => {
    console.log(`Listenting at ${server.address().address}:${server.address().port}`)
});