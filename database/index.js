const { Sequelize } = require('sequelize');
const UserModel = require('./models/user.model');
const EventModel = require('./models/event.model');


// if(process.env.DB_HOST) {
//     const dbSocketAddr = process.env.DB_HOST.split(':');
//     const sequalize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
//         // host: dbSocketAddr[0],
//         // port: dbSocketAddr[1],
//         dialect: 'mysql',
//         dialectOptions: {
//             socketPath: `${dbSocketPath}/${process.env.CLOUD_SQL_CONNECTION_NAME}`
//         }
        
//     });
// }

const dbSocketPath = process.env.DB_SOCKET_PATH || '/cloudsql';

const sequalize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    // host: dbSocketAddr[0],
    // port: dbSocketAddr[1],
    dialect: 'mysql',
    dialectOptions: {
        socketPath: `${dbSocketPath}/${process.env.CLOUD_SQL_CONNECTION_NAME}`
    }
    
});

const Event = EventModel(sequalize);
const User = UserModel(sequalize);

Event.belongsToMany(User, {
    foreignKey: "event_id",
    as: "users",
    through: "user_event"
});
User.belongsToMany(Event, {
    through: 'user_event',
    as: "events",
    foreignKey: "user_id"
});

sequalize.sync({force: false})
// .then(() =>{
    
// User.findByPk(1, {
//     include: [{
//         model: Event,
//         attributes: ['id', 'name'],
//         as: 'events'
//     }]
// }).then((user) => {
//     console.log(user.toJSON())
//     Event.findByPk(1).then((ev) => {
//         user.addEvents(ev);
//     })
// })

// })

module.exports = {
    User,
    Event
}