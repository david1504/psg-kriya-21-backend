const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequalize = new Sequelize()) => {
    return sequalize.define('user', {
        id: {
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        email: {
            type: DataTypes.CHAR(100),
            unique: true,
            allowNull: false
        },
        firstName: {
            type: DataTypes.CHAR(100)
        },
        lastName: {
            type: DataTypes.CHAR(100)
        },
        college: {
            type: DataTypes.CHAR,
            allowNull: false
        },
        profileCompleted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    });
}