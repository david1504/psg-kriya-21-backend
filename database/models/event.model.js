const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequalize = new Sequelize()) => {
    return sequalize.define('event', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        name: {
            type: DataTypes.CHAR(100),
            unique: true
        }
    }, {
        timestamps: false
    });
}