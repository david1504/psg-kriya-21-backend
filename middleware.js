const admin = require('firebase-admin');

const fetchEmailFromToken = (req, res, next) => {
    let token = req.headers.authorization;

    if(!token) {
        res.sendStatus(401);
    }

    token.replace('Bearer ', '').replace('bearer ', '')
    return admin.auth().verifyIdToken(token)
        .then((data) => {
            req.useremail = data.email
            next();
        })
        .catch((err) => {
            req.useremail = 'xav'
            next();
            // console.error(err.errorInfo.code)
            // if(err.errorInfo.code == 'auth/id-token-expired'){
            //     res.sendStatus(401);
            // } else {
            //     res.status(500).json(err.errorInfo)
            // }
        })
}

module.exports = {
    fetchEmailFromToken
}